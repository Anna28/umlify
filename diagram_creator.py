from extractor import Extractor
import matplotlib.pyplot as plt
from numpy import arange
from graphviz import ExecutableNotFound
from subprocess import CalledProcessError
from abc import ABCMeta, abstractmethod
from component_viewer import DotCreator

# classes that handle different types of diagram and chart creation


class DiagramCreator(metaclass=ABCMeta):
    def __init__(self, view, output_file_name=None, extractor=None):
        self.output_path = ''
        self.v = view
        self.output_file_name = output_file_name
        self.e = extractor

    @abstractmethod
    def generate_diagram(self, comp_name=None):  # pragma: no cover
        pass

    def set_extractor(self, new_extractor: Extractor):
        self.e = new_extractor

    def set_output_path(self, new_path):
        self.output_path = new_path

    def set_output_name(self, new_name):
        self.output_file_name = new_name

    @classmethod
    def _get_attribute_types(cls, component):
        att_types = {"int": 0, "str": 0, "dict": 0, "list": 0, "bool": 0, "none": 0, "tuple": 0, "obj": 0}
        if component is not None:
            attributes = component.get_attributes()

            if attributes is not None and len(attributes) > 0:
                for attrib in attributes:
                    for a in attributes[attrib]:
                        if a is not '':
                            att_types[a] = att_types[a] + 1
        return att_types

    @classmethod
    def _remove_by_key(cls, dictionary, key):
        r = dict(dictionary)
        del r[key]
        return r


class PieChartCreator(DiagramCreator):
    def generate_diagram(self, comp_name=None):
        # ensuring the class name passed in starts with an uppercase letter
        comp_name = comp_name[0].upper() + comp_name[1:]
        if self.output_file_name is None:
            self.output_file_name = comp_name + '-pie-chart'

        the_component = self.e.get_component_dictionary().get(comp_name)
        att_types = self._get_attribute_types(the_component)
        removable = []
        for k, v in att_types.items():
            if v == 0:
                removable.append(k)
        for r in removable:
            att_types = self._remove_by_key(att_types, r)

        if len(att_types) > 0:
            labels = att_types.keys()
            sizes = att_types.values()
            plt.pie(sizes, labels=labels, autopct='%1.1f%%')
            plt.title("Percentage of variable data types")
            plt.axis('equal')
            plt.savefig(self.output_path+self.output_file_name+".png")
            plt.gcf().clear()


class BarChartCreator(DiagramCreator):
    def generate_diagram(self, comp_name=None):
        if self.output_file_name is None:
            self.output_file_name = 'bar-chart'
        bar_width = 0.35
        opacity = 0.8
        objects = tuple(self.e.get_component_dictionary().keys())
        if len(objects) > 0:
            no_of_attributes = []
            no_of_functions = []
            for key in objects:
                component = self.e.get_component_dictionary().get(key)
                no_of_attributes.append(len(component.get_attributes()))
                no_of_functions.append(len(component.get_functions()))
            y_pos = arange(len(objects))

            plt.bar(y_pos, no_of_attributes, bar_width, alpha=opacity, color='b', label='attributes')
            plt.bar(y_pos + bar_width, no_of_functions, bar_width, alpha=opacity, color='g', label='functions')
            plt.xticks(y_pos + bar_width, objects, rotation=90)
            plt.subplots_adjust(bottom=0.50)
            plt.ylabel('Count')
            plt.xlabel('Class Name')
            plt.title('Number of Functions and Attributes per Class')
            plt.legend()

            plt.savefig(self.output_path+self.output_file_name + ".png")
            plt.gcf().clear()
        else:
            self.v.display_err("unable to generate bar chart, no components found")


class ClassDiagramCreator(DiagramCreator, DotCreator):
    def generate_diagram(self, comp_name=None):
        dot = None
        dot = None
        if self.output_file_name is None:
            self.output_file_name = 'class-diagram'
        output = self.output_file_name
        if self.output_path is not None:
            output = self.output_path + output

        try:
            dot = self._initialise_diagram('UML Class Diagram', 'dot')
        except ExecutableNotFound:  # pragma: no cover
            self.v.display_err("Graphviz executable not found, please check it is properly installed.")
        except (CalledProcessError, RuntimeError) as err:  # pragma: no cover
            self.v.display_err("An unexpected error occurred")
            self.v.display_err(err)
        components = list(self.e.get_component_dictionary().values())
        if (len(components) > 0) & (dot is not None):
            for comp in components:
                dot = self._add_comp_to_diagram(dot, comp)
                dot = self._add_parents_to_diagram(dot, comp)
            dot.render(output, view=False)
            self.write_dot_to_png(dot_file=output)
            self._remove_temp_files(output)
        else:
            self.v.display_err("unable to add components to diagram.")
