"""
This module handles the command line input and help messages for Umlify
"""

import sys
import os
from cmd import Cmd
from shelf import Shelf
from database import Database
from umlify_component_viewer import UmlifyComponentViewer


class CommandLine(Cmd):
    """
    CommandLine class that uses Cmd
    Instead of a single command with multiple options (eg `umlify -d ./myclasses/ -o image.png`
    it will have multiple commands to set each value
    """

    def __init__(self, testing=False):
        Cmd.__init__(self)
        self.prompt = "Umlify> "
        self.intro = "Welcome to Umlify. Use \"help\" for help."
        self.input_path = None
        self.output_path = None
        self.allowed_types = ["dot", "png", "pdf"]
        # default output file type .dot (first in allowed types)
        self.output_file_type = self.allowed_types[0]
        self.run = False
        self.cv = None
        self.db = Database()
        stdout_old = sys.stdout
        sys.stdout = open(os.devnull, "w")
        self.db.create_connection("uml_components.db")
        sys.stdout = stdout_old
        if len(sys.argv) > 1 and not testing:
            self.start(sys.argv)  # pragma: no cover
        elif not testing:
            # only run cmdloop if not using commandline arguments
            self.cmdloop()  # pragma: no cover

    def start(self, params):
        """
        >>> cl = CommandLine(True)
        >>> args = "".split()
        >>> cl.start([sys.argv[0]] + args)
        Please select an input path with "-f <file>"
        Quitting Umlify.

        >>> args = "-f test/file_does_not_exist.py".split()
        >>> cl.start([sys.argv[0]] + args)
        Input file set to "test/file_does_not_exist.py"
        Running Umlify...
        File not found
        Generating class diagram
        unable to add components to diagram.
        Quitting Umlify.

        >>> args = "-f test/test_class_8.py -b".split()
        >>> cl.start([sys.argv[0]] + args)
        Input file set to "test/test_class_8.py"
        Running Umlify...
        Generating class diagram
        unable to add components to diagram.
        unable to generate bar chart, no components found
        Generating a bar chart
        Quitting Umlify.

        >>> args = "-f test/test_class_1.py".split()
        >>> cl.start([sys.argv[0]] + args)
        Input file set to "test/test_class_1.py"
        Running Umlify...
        Generating class diagram
        Quitting Umlify.

        >>> args = "-f .".split()
        >>> cl.start([sys.argv[0]] + args)
        Input file set to "."
        Running Umlify...
        Generating class diagram
        Quitting Umlify.

        >>> args = "-f test/test_class_5.py -b".split()
        >>> cl.start([sys.argv[0]] + args)
        Input file set to "test/test_class_5.py"
        Running Umlify...
        Generating class diagram
        Generating a bar chart
        Quitting Umlify.

        >>> args = "-f test/test_class_5.py -p Sunflower".split()
        >>> cl.start([sys.argv[0]] + args)
        Input file set to "test/test_class_5.py"
        Running Umlify...
        Generating class diagram
        Generating a pie chart for component: Sunflower
        Quitting Umlify.

        >>> args = "-f test/test_class_5.py -p Orchid -b".split()
        >>> cl.start([sys.argv[0]] + args)
        Input file set to "test/test_class_5.py"
        Running Umlify...
        Generating class diagram
        Generating a bar chart
        Generating a pie chart for component: Orchid
        Quitting Umlify.

        >>> args = "-f test/test_class_5.py -p".split()
        >>> cl.start([sys.argv[0]] + args)
        Input file set to "test/test_class_5.py"
        Running Umlify...
        Generating class diagram
        Generating pie charts for all components
        Quitting Umlify.

        >>> args = "-f test/test_class_5.py -p Orchid orchid_file".split()
        >>> cl.start([sys.argv[0]] + args)
        Input file set to "test/test_class_5.py"
        Running Umlify...
        Generating class diagram
        Generating a pie chart for component: Orchid
        Quitting Umlify.

        >>> args = "-f".split()
        >>> cl.start([sys.argv[0]] + args)
        Traceback (most recent call last):
            ...
        Exception: You must provide a file name with "-f"


        >>> args = "-h".split()
        >>> cl.start([sys.argv[0]] + args)
        Generates UML diagrams of Python code
        <BLANKLINE>
        Umlify command usage:
          commandline.py -f <file> [-h|-b [output_file]|-p [comp_name] [output_file]]
        <BLANKLINE>
          -f <file> : specifies the Python file to generate diagrams from
              a class diagram is generated when the program is run
        <BLANKLINE>
          -b [output_file] : generates a bar chart
              using "output_file" sets the output file for the bar chart
        <BLANKLINE>
          -p [comp_name] [output_file] : generates pie charts for all components
              using "comp_name" generates a pie chart only for comp_name
              using "output_file" sets the output file for the "comp_name" pie chart
        <BLANKLINE>
          -h : displays this help message

        Usage: start -f <file> [-bc [output_file]|-pc [comp_name] [output_file]]
        :param params: The commandline arguments and inputs
        :return:
        """
        # command help
        if "-h" in params or "-help" in params:
            print("""Generates UML diagrams of Python code
            
Umlify command usage:
  commandline.py -f <file> [-h|-b [output_file]|-p [comp_name] [output_file]]

  -f <file> : specifies the Python file to generate diagrams from
      a class diagram is generated when the program is run

  -b [output_file] : generates a bar chart
      using \"output_file\" sets the output file for the bar chart

  -p [comp_name] [output_file] : generates pie charts for all components
      using \"comp_name\" generates a pie chart only for comp_name
      using \"output_file\" sets the output file for the \"comp_name\" pie chart

  -h : displays this help message""")  # .format(file=params[0]))

            return

        # file setting
        if "-f" in params:
            try:
                file = params[params.index("-f") + 1]
            except IndexError:
                raise Exception("You must provide a file name with \"-f\"")
            self.do_file(file)
            self.do_run()
            self.do_class_diagram()
        else:
            print("Please select an input path with \"-f <file>\"")
            self.do_quit()
            return

        # bar chart
        if "-b" in params:
            try:
                bc_output_file = params[params.index("-b") + 1]
            except IndexError:
                bc_output_file = None

            if bc_output_file is None or bc_output_file[0] == "-":
                # this is another option, don't use it
                bc_output_file = None

            self.do_bar_chart(bc_output_file)

        if "-p" in params:
            try:
                comp_name = params[params.index("-p") + 1]
            except IndexError:
                comp_name = None
            # This is separate because comp_name can be given without pc_output_file
            try:
                pc_output_file = params[params.index("-p") + 2]
            except IndexError:
                pc_output_file = None

            if comp_name is None or comp_name[0] == "-":
                pc_params = None
            elif pc_output_file is None or pc_output_file[0] == "-":
                pc_params = comp_name
            else:
                pc_params = comp_name + " " + pc_output_file

            self.do_pie_chart(pc_params)

        self.do_quit()

    @staticmethod
    def help_run():
        """
        >>> cl = CommandLine(True)
        >>> cl.help_run()
        <BLANKLINE>
        Usage: run [output_file_name]
        Runs Umlify with the current settings, use other commands to change them
        :param output_file_name: the file name for the diagram to output to
        :return:
        <BLANKLINE>
        """
        print("""
Usage: run [output_file_name]
Runs Umlify with the current settings, use other commands to change them
:param output_file_name: the file name for the diagram to output to
:return:
""")

    def do_run(self, line=None):
        """
        >>> cl = CommandLine(True)
        >>> cl.do_run()
        Please select an input path with "file"
        >>> cl.do_file("test/test_class_7.py")
        Input file set to "test/test_class_7.py"
        >>> cl.do_run()
        Running Umlify...
        >>> cl.do_class_diagram()
        Generating class diagram
        """

        if not self.input_path:
            print("Please select an input path with \"file\"")
            return

        print("Running Umlify...")
        self.cv = UmlifyComponentViewer(self.input_path, self.output_path)
        self.run = True

    @staticmethod
    def help_class_diagram():
        """
        >>> cl = CommandLine(True)
        >>> cl.help_class_diagram()
        <BLANKLINE>
        Usage: class_diagram [output_file_name]
        Generate a class diagram
        :param output_file_name: the file name for the diagram to output to
        :return:
        <BLANKLINE>
        """
        print("""
Usage: class_diagram [output_file_name]
Generate a class diagram
:param output_file_name: the file name for the diagram to output to
:return:
""")

    def do_class_diagram(self, output_file_name=None):
        """
        >>> cl = CommandLine(True)
        >>> cl.do_file("test/test_class_7.py")
        Input file set to "test/test_class_7.py"
        >>> cl.do_class_diagram()
        Please use \"run\" before trying to create a chart
        >>> cl.do_run()
        Running Umlify...
        >>> cl.do_class_diagram()
        Generating class diagram
        >>> cl.do_class_diagram("test_diagram1")
        Generating class diagram with name "test_diagram1"
        >>> cl.do_class_diagram("test_diagram2.png")
        Generating class diagram with name "test_diagram2"
        >>> cl.do_class_diagram("test_diagram3.dot")
        Generating class diagram with name "test_diagram3"
        """
        if not self.run:
            print("Please use \"run\" before trying to create a chart")
            return

        if not output_file_name:
            output_file_name = None
            print("Generating class diagram")
        else:
            # only display the name of the file and not the extension
            print("Generating class diagram with name \"{name}\"".format(name=output_file_name.rsplit('.', 1)[0]))
        self.cv.generate_class_diagram(output_file_name)

    @staticmethod
    def help_bar_chart():
        """
        >>> cl = CommandLine(True)
        >>> cl.help_bar_chart()
        <BLANKLINE>
        Usage: bar_chart [output_file_name]
        Generate a bar chart about classes
        :param output_file_name: the file name for the chart to output to
        :return:
        <BLANKLINE>
        """
        print("""
Usage: bar_chart [output_file_name]
Generate a bar chart about classes
:param output_file_name: the file name for the chart to output to
:return:
""")

    def do_bar_chart(self, output_file_name=None):
        """
        >>> cl = CommandLine(True)
        >>> cl.do_bar_chart()
        Please use "run" before trying to create a chart

        >>> cl.do_file("test/test_class_6.py")
        Input file set to "test/test_class_6.py"
        >>> cl.do_run()
        Running Umlify...
        >>> cl.do_bar_chart()
        Generating a bar chart
        """
        if not self.run:
            print("Please use \"run\" before trying to create a chart")
            return
        else:
            if not output_file_name:
                output_file_name = None
            self.cv.generate_bar_chart(output_file_name)
            print("Generating a bar chart")

    @staticmethod
    def help_pie_chart():
        """
        >>> cl = CommandLine(True)
        >>> cl.help_pie_chart()
        <BLANKLINE>
        Usage: pie_chart [comp_name] [output_file_name]
        Makes a pie chart about a class. Give no input for pie charts of every component
        :param params: the input which includes optional values comp_name and output_file_name
        comp_name the name of the component to make a chart of
        output_file_name the file name for the chart to output to
        :return:
        <BLANKLINE>
        """
        print("""
Usage: pie_chart [comp_name] [output_file_name]
Makes a pie chart about a class. Give no input for pie charts of every component
:param params: the input which includes optional values comp_name and output_file_name
comp_name the name of the component to make a chart of
output_file_name the file name for the chart to output to
:return:
""")

    def do_pie_chart(self, params=None):
        """
        >>> cl = CommandLine(True)
        >>> cl.do_pie_chart()
        Please use "run" before trying to create a chart

        >>> cl.do_file("test/test_class_5.py")
        Input file set to "test/test_class_5.py"
        >>> cl.do_run()
        Running Umlify...
        >>> cl.do_pie_chart()
        Generating pie charts for all components
        >>> cl.do_pie_chart("Sunflower")
        Generating a pie chart for component: Sunflower
        """
        if not self.run:
            print("Please use \"run\" before trying to create a chart")
            return

        if not params:
            print("Generating pie charts for all components")
            self.cv.generate_pie_charts()
            return
        if " " in params:
            # split params into component name and output file
            comp_name, output_file_name = params.split(' ')
        else:
            comp_name = params
            output_file_name = None

        # example of comp_name: "Herbivore"
        print("Generating a pie chart for component: {comp_name}".format(comp_name=comp_name))
        self.cv.generate_pie_chart(comp_name, output_file_name)

    # def do_validate(self, line):
    #    """
    #    Validates a file without passing it to be drawn
    #    :return: None
    #    """
    #
    #    pass

    @staticmethod
    def help_file():
        """
        >>> cl = CommandLine(True)
        >>> cl.help_file()
        <BLANKLINE>
        Usage: file <file>
        Selects a file as input to Umlify
        :param file: the name of the file to be used as input
        :return: None
        <BLANKLINE>
        """
        print("""
Usage: file <file>
Selects a file as input to Umlify
:param file: the name of the file to be used as input
:return: None
""")

    def do_file(self, file):
        """
        >>> cl = CommandLine(True)
        >>> cl.do_file(None)
        Please enter a file to use as input
        >>> cl.do_file("test/test_class_1.py")
        Input file set to "test/test_class_1.py"
        """
        if file:
            self.input_path = file
            print("Input file set to \"{file}\"".format(file=file))
        else:
            print("Please enter a file to use as input")

#     @staticmethod
#     def help_directory():
#         print("""
# Usage: directory <directory>
# Selects a directory as input to Umlify
# :param directory: the name of the directory holding files to be used as input
# :return: None
# """)

#    def do_directory(self, directory):
#
#        if directory:
#            self.input_path = directory
#        else:
#            print("Please enter a directory to use as input")

    @staticmethod
    def help_location():
        """
        >>> cl = CommandLine(True)
        >>> cl.help_location()
        <BLANKLINE>
        Usage: location <location>
        Selects a destination location for the output files
        :param location:
        :return: None
        <BLANKLINE>
        """
        print("""
Usage: location <location>
Selects a destination location for the output files
:param location:
:return: None
""")

    def do_location(self, location=None):
        r"""
        >>> cl = CommandLine(True)
        >>> cl.do_file("test/test_class_7.py")
        Input file set to "test/test_class_7.py"
        >>> cl.do_location()
        Please enter a location for output files to be saved
        >>> cl.do_location("output/test_output1/")
        Output location set to "output/test_output1/"
        >>> cl.do_run()
        Running Umlify...
        >>> cl.do_class_diagram()
        Generating class diagram
        >>> cl.do_location("output/test_output2")
        Output location set to "output/test_output2"
        >>> cl.do_run()
        Running Umlify...
        >>> cl.do_pie_chart()
        Generating pie charts for all components
        >>> cl.do_location("output\\test_output3")
        Output location set to "output\test_output3"
        >>> cl.do_run()
        Running Umlify...
        >>> cl.do_bar_chart()
        Generating a bar chart
        """
        if not location:
            print("Please enter a location for output files to be saved")
            return

        print("Output location set to \"{location}\"".format(location=location))
        if not location.endswith("/") and not location.endswith("\\"):
            # make sure it is a directory
            if "\\" in location:
                location = location + "\\"
            else:
                location = location + "/"

        self.output_path = location

    def help_type(self):
        """
        >>> cl = CommandLine(True)
        >>> cl.help_type()
        <BLANKLINE>
        Usage: type <file_type>
        Sets the output file type
        Allowed types: dot, png, pdf
        :param file_type: a string representing a file extension
        :return: None
        <BLANKLINE>
        """
        print("""
Usage: type <file_type>
Sets the output file type
Allowed types: {file_types}
:param file_type: a string representing a file extension
:return: None
""".format(file_types=', '.join(self.allowed_types)))

    def do_type(self, file_type):
        """
        >>> cl = CommandLine(True)
        >>> cl.do_type(None)
        Please enter a file type
        >>> cl.do_type("png")
        Output file type has been set to png
        >>> cl.do_type("png")
        The output file type is already png
        >>> cl.do_type("pdf")
        Output file type has been set to pdf
        >>> cl.do_type(".png")
        Output file type has been set to png
        >>> cl.do_type(".DOT")
        Output file type has been set to dot
        >>> cl.do_type("invalid_type")
        That file type is not supported, please pick one from: dot, png, pdf
        """
        if not file_type:
            # no file type was provided
            print("Please enter a file type")
            return

        file_type = file_type.lower()  # convert to all lowercase

        # remove leading . if given (eg .png instead of png)
        if file_type[0] == ".":
            file_type = file_type[1:]

        if file_type == self.output_file_type:
            print("The output file type is already {file_type}".format(file_type=file_type))
        elif file_type in self.allowed_types:
            self.output_file_type = file_type
            print("Output file type has been set to {file_type}".format(file_type=file_type))
        else:
            # input provided was not in the list of allowed types
            print("That file type is not supported, please pick one from: {file_types}".format(
                file_types=', '.join(self.allowed_types)))

    @staticmethod
    def help_shelf():
        """
        >>> cl = CommandLine(True)
        >>> cl.help_shelf()
        <BLANKLINE>
        Writes and reads Component objects extracted from the currently selected file to [filename].dat
        <BLANKLINE>
        Syntax: shelf [flag]
        shelf -w <filename>: writes the class components of selected file to [filename].dat
        shelf -r <filename>: reads the contents of shelve file
        :param flag: <-w|-r> <filename>
        :return: shelf contents
        <BLANKLINE>
        """
        print("""
Writes and reads Component objects extracted from the currently selected file to [filename].dat

Syntax: shelf [flag]
shelf -w <filename>: writes the class components of selected file to [filename].dat
shelf -r <filename>: reads the contents of shelve file
:param flag: <-w|-r> <filename>
:return: shelf contents
""")

    def do_shelf(self, line=None):
        """
        >>> cl = CommandLine(True)
        >>> cl.do_shelf()
        Please select an input path with \"file\"
        >>> cl.do_file("test/test_class_2.py")
        Input file set to "test/test_class_2.py"
        >>> cl.do_shelf()
        Please provide a valid flag
        >>> cl.do_shelf("-a")
        Please provide a valid flag
        >>> cl.do_shelf("-w")
        You must provide a file name with "-w"
        >>> cl.do_shelf("-w test/test_shelf")
        Class components of [test/test_class_2.py] serialized and stored to file successfully
        >>> cl.do_shelf("-r")
        You must provide a file name with "-r"
        >>> cl.do_shelf("-r test/test_shelf")
        Class Name: Person
        Attribute: name
        Attribute: age
        Function: __init__
        --------------------
        <BLANKLINE>
        >>> cl.do_shelf("-r invalid_file")
        File Error: 'invalid_file' does not exist!
        """
        if not self.input_path:
            print("Please select an input path with \"file\"")
            return

        if not line or line == "":
            print("Please provide a valid flag")
            return

        shelf = Shelf(self.input_path)

        params = line.split(" ")

        if "-w" in params:
            try:
                filename = params[params.index("-w") + 1]
            except IndexError:
                print("You must provide a file name with \"-w\"")
                return
            shelf.write_shelf(filename)
        elif "-r" in params:
            try:
                filename = params[params.index("-r") + 1]
            except IndexError:
                print("You must provide a file name with \"-r\"")
                return
            try:
                print(shelf.read_shelf(filename))
            except Exception as e:
                print(e)
        else:
            print("Please provide a valid flag")

    @staticmethod
    def help_database():
        """
        >>> cl = CommandLine(True)
        >>> cl.help_database()
        <BLANKLINE>
        Save and read class component data with a database. Stores component object as a pickle file.
        <BLANKLINE>
        Syntax: database <flag> [filename]
        database: displays all data and the number of components stored in the database
        database -i: inserts the class components of currently selected file to the database
        database -v <filename>: displays the class components of a certain filename within the database
        <BLANKLINE>
        :param flag: -i, -v <filename>
        :return: db values
        <BLANKLINE>
        """
        print("""
Save and read class component data with a database. Stores component object as a pickle file.

Syntax: database <flag> [filename]
database: displays all data and the number of components stored in the database
database -i: inserts the class components of currently selected file to the database
database -v <filename>: displays the class components of a certain filename within the database

:param flag: -i, -v <filename>
:return: db values
""")

    def do_database(self, line=None):
        """
        >>> cl = CommandLine(True)
        >>> cl.do_database()
        Please select an input path with \"file\"
        >>> cl.do_file("test/test_class_2.py")
        Input file set to "test/test_class_2.py"
        >>> cl.do_database()
        The number of components in the database: 0
        >>> cl.do_database("-a")
        Please provide a valid flag
        >>> cl.do_database("-i")
        Inserted data
        >>> cl.do_database("-v")
        You must provide a file name with "-v"
        >>> cl.do_database("-v test/test_class_2.py")
        Class Name:Person
        Attribute:{'name': {'str': 'new_name'}, 'age': {'str': 'new_age'}}
        Function:['__init__']
        <BLANKLINE>
        >>> cl.do_database("-v test/test_class_2")
        Class Name:Person
        Attribute:{'name': {'str': 'new_name'}, 'age': {'str': 'new_age'}}
        Function:['__init__']
        <BLANKLINE>
        >>> cl.do_database()
        Class Name:Person
        Attribute:{'name': {'str': 'new_name'}, 'age': {'str': 'new_age'}}
        Function:['__init__']
        --------------------
        The number of components in the database: 1
        """
        if not self.input_path:
            print("Please select an input path with \"file\"")
            return

        if not line or line == "":
            print(self.db.get_all())
            return

        params = line.split(" ")

        if "-i" in params:
            self.db.insert_data(self.input_path)
            print("Inserted data")
        elif "-v" in params:
            try:
                filename = params[params.index("-v") + 1]
            except IndexError:
                print("You must provide a file name with \"-v\"")
                return
            print(self.db.get_specific(filename))
        else:
            print("Please provide a valid flag")

    @staticmethod
    def help_quit():
        """
        >>> cl = CommandLine(True)
        >>> cl.help_quit()
        <BLANKLINE>
        Usage: quit
        Quit Umlify
        :return: True
        <BLANKLINE>
        """
        print("""
Usage: quit
Quit Umlify
:return: True
""")

    @staticmethod
    def do_quit(line=None):
        """
        >>> cl = CommandLine(True)
        >>> cl.do_quit()
        Quitting Umlify.
        True
        """
        print("Quitting Umlify.")
        return True

    # command aliases
    do_r = do_run
    help_r = help_run
    do_c = do_class_diagram
    help_c = help_class_diagram
    do_b = do_bar_chart
    help_b = help_bar_chart
    do_p = do_pie_chart
    help_p = help_pie_chart
    # do_v = do_validate
    do_f = do_file
    help_f = help_file
    # do_d = do_directory
    # help_d = help_directory
    do_l = do_location
    help_l = help_location
    do_t = do_type
    help_t = help_type
    do_s = do_shelf
    help_s = help_shelf
    do_db = do_database
    help_db = help_database
    do_q = do_quit
    help_q = help_quit


if __name__ == "__main__":
    command_line = CommandLine()  # pragma: no cover
    # import doctest
    # doctest.testmod(verbose=True)
