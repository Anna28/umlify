import unittest
import os
from shelf import Shelf
import shelve


class ShelfUnitTest(unittest.TestCase):
    """Unittests for Shelf"""
    def setUp(self):
        self.shelf_name = "test_shelf"

    def test_write_shelf(self):
        # Arrange
        shelf = Shelf("test_class_2.py")
        # to ensure that shelf file is empty before act
        shelf.clear_shelf(self.shelf_name)

        # Act
        shelf.write_shelf(self.shelf_name)
        with shelve.open(self.shelf_name) as shelf:
            actual = bool(shelf)

        # Assert
        self.assertTrue(actual)

    def test_read_shelf(self):
        # Arrange
        shelf = Shelf("test_class_5.py")
        shelf.clear_shelf(self.shelf_name)
        shelf.write_shelf(self.shelf_name)

        expected = "Class Name: Plant\n"
        expected += "Attribute: plant_height\n"
        expected += "Function: __init__\n"
        expected += "Function: grow_plant\n"
        expected += "-"*20 + "\n"
        expected += "Class Name: Sunflower\n"
        expected += "Function: drop_seed\n"
        expected += "Parent: Plant\n"
        expected += "-"*20 + "\n"
        expected += "Class Name: Orchid\n"
        expected += "Parent: Plant\n"
        expected += "-"*20 + "\n"

        # Act
        actual = shelf.read_shelf(self.shelf_name)

        # Assert
        self.assertMultiLineEqual(expected, actual)

    def test_read_shelf_components_not_found(self):
        # Arrange
        shelf = Shelf("test_class_6.py")
        shelf.write_shelf(self.shelf_name)
        shelf.clear_shelf(self.shelf_name)
        expected = "Components of 'test_class_6' cannot be found in the file"

        # Act
        actual = shelf.read_shelf(self.shelf_name)

        # Assert
        self.assertEqual(expected, actual)

    def test_clear_shelf(self):
        # Arrange
        shelf = Shelf("test_class_6.py")
        shelf.clear_shelf(self.shelf_name)
        expected = []

        # Act
        with shelve.open(self.shelf_name, 'r') as test_file:
            actual = list(test_file.keys())

        # Assert
        self.assertListEqual(expected, actual)

    def tearDown(self):
        os.remove(self.shelf_name + ".dat")
        os.remove(self.shelf_name + ".bak")
        os.remove(self.shelf_name + ".dir")


if __name__ == '__main__':  # pragma: no cover
    unittest.main(verbosity=2)
    # unittest.main()
