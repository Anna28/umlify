from unittest import TestCase, main
from umlify_component_viewer import UmlifyComponentViewer
from extractor import Extractor
from component import Component
from View import ConsoleView


class CVTests(TestCase):
    """Tests for component_viewer.py"""

    def setUp(self):
        self.test_file = "test_class_5.py"
        self.ucv = UmlifyComponentViewer(ConsoleView(), self.test_file)

    def test_file_path(self):
        self.assertEqual(self.test_file, self.ucv.input_path)

    def test_path_provided(self):
        self.assertTrue(self.ucv.path_provided)

    def test_extractor(self):
        self.assertIsInstance(self.ucv.e, Extractor)

    def test_components_not_none(self):
        self.assertIsNotNone(self.ucv.components)

    def test_components_not_empty(self):
        self.assertIsNot(len(self.ucv.components), 0)

    def test_components_are_list(self):
        self.assertIsInstance(self.ucv.components, list)

    def test_components_list_components(self):
        comps = self.ucv.components
        test_item = comps[0]
        self.assertIsInstance(test_item, Component)


if __name__ == '__main__':  # pragma: no cover
    main(verbosity=2)
