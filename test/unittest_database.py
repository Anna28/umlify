import unittest
from database import Database
import sqlite3 as sql
import os


class DatabaseUnitTest(unittest.TestCase):
    """Unittests for Database"""
    def setUp(self):
        self.db = Database()
        self.db_name = "test_case.db"

    def test_database_created(self):
        # Arrange
        self.db.create_connection(self.db_name)

        # Act
        actual = os.path.isfile("test_case.db")

        # Assert
        self.assertTrue(actual)

    def test_table_created(self):
        # Arrange
        self.db._conn = sql.connect(self.db_name)
        self.db._cursor = self.db._conn.cursor()
        self.db.drop_table()
        self.db.create_table()

        # Act
        statement = "SELECT name FROM sqlite_master WHERE type='table'"
        self.db._cursor.execute(statement)
        result = bool(self.db._cursor.fetchone())

        # Assert
        self.assertTrue(result)

        self.db._conn.close()

    def test_add_to_database(self):
        # Arrange
        self.db.create_connection(self.db_name)
        expected = "Class Name:Person\n"
        expected += "Attribute:{'name': {'str': 'new_name'}, 'age': {'str': 'new_age'}}\n"
        expected += "Function:['__init__']\n"

        # Act
        self.db.insert_data('test_class_2.py')
        actual = self.db.get_specific('test_class_2.py')

        # Assert
        self.assertEqual(expected, actual)

    def test_get_all(self):
        # Arrange
        self.db.create_connection(self.db_name)
        expected = "Class Name:Plant\n"
        expected += "Attribute:{'plant_height': {'str': 'h'}}\n"
        expected += "Function:['__init__', 'grow_plant']\n"
        expected += "-" * 20 + "\n"
        expected += "Class Name:Sunflower\n"
        expected += "Attribute:[]\n"
        expected += "Function:['drop_seed']\n"
        expected += "Parent:Plant\n"
        expected += "-" * 20 + "\n"
        expected += "Class Name:Orchid\n"
        expected += "Attribute:[]\n"
        expected += "Function:[]\n"
        expected += "Parent:Plant\n"
        expected += "-" * 20 + "\n"
        expected += "Class Name:Person\n"
        expected += "Attribute:{'name': {'str': 'new_name'}, 'age': {'str': 'new_age'}}\n"
        expected += "Function:['__init__']\n"
        expected += "-" * 20 + "\n"
        expected += "The number of components in the database: 4"

        # Act
        self.db.insert_data('test_class_5.py')
        self.db.insert_data('test_class_2.py')
        actual = self.db.get_all()

        # Assert
        self.assertEqual(expected, actual)

    def tearDown(self):
        os.remove(self.db_name)


if __name__ == '__main__':  # pragma: no cover
    unittest.main(verbosity=2)
