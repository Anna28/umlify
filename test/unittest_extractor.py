import unittest
from extractor import Extractor

"""Ten unit tests to test the extractor.py class, primarily the regex extractions"""


class MainTests(unittest.TestCase):
    """Tests for extractor.py`."""

    def test_class_extraction(self):
        """A class name can be extracted from line of code"""
        # Arrange
        e = Extractor()
        expected = 'Marsupial'
        line = "class Marsupial(Mammal):"

        # Act
        actual = e._extract_class(line)

        # Assert
        self.assertEqual([expected], actual)

    def test_nil_class_extraction(self):
        """A name will not be extracted from line of code where no class present"""
        # Arrange
        e = Extractor()
        expected = []
        line = "class(Mammal):"

        # Act
        actual = e._extract_class(line)

        # Assert
        self.assertEqual(expected, actual)

    def test_function_extraction(self):
        """A function name can be extracted from line of code"""
        # Arrange
        e = Extractor()
        expected = 'proliferates'
        line = "def proliferates(self):"

        # Act
        actual = e. _extract_functions(line)

        # Assert
        self.assertEqual([expected], actual)

    def test_nil_function_extraction(self):
        """A name will not be extracted from line of code where no function present"""
        # Arrange
        e = Extractor()
        expected = []
        line = "    teeth = 'Sharp'"

        # Act
        actual = e._extract_class(line)

        # Assert
        self.assertEqual(expected, actual)

    def test_attribute_extraction(self):
        """An attribute name can be extracted from line of code"""
        # Arrange
        e = Extractor()
        expected = 'teeth'
        line = "    self.teeth = 'Sharp'"

        # Act
        actual = e._extract_attributes(line)

        # Assert
        self.assertEqual([expected], actual)

    def test_nil_attribute_extraction(self):
        """A name will not be extracted from line of code where no attribute present"""
        # Arrange
        e = Extractor()
        expected = []
        line = "    self.= 'Sharp'"

        # Act
        actual = e._extract_class(line)

        # Assert
        self.assertEqual(expected, actual)

    def test_default_value_extraction(self):
        """An attribute default value can be extracted from line of code"""
        # Arrange
        e = Extractor()
        expected = 'Sharp'
        line = "    self.teeth = Sharp"

        # Act
        actual = e._extract_attribute_defaults(line)

        # Assert
        self.assertEqual(expected, actual)

    def test_nil_default_value_extraction(self):
        """A value will not be extracted from line of code where no default present"""
        # Arrange
        e = Extractor()
        expected = []
        line = "    self.teeth ='"

        # Act
        actual = e._extract_class(line)

        # Assert
        self.assertEqual(expected, actual)

    def test_data_type_extraction(self):
        """An attribute data type can be extracted from line of code"""
        # Arrange
        e = Extractor()
        expected = 'str'
        line = "Sharp"

        # Act
        actual = e._extract_attribute_datatypes(line)

        # Assert
        self.assertEqual(expected, actual)

    @unittest.skip('This is not functioning yet.')
    def test_unknown_data_type_extraction(self):  # pragma: no cover
        """An error message is raised when an unknown attribute data type is extracted from line of code"""
        # Arrange
        e = Extractor()
        expected = "No data type detected for '@'"
        line = '@'

        # Act
        actual = e._extract_attribute_datatypes(line)

        # Assert
        self.assertEqual(expected, actual)


if __name__ == '__main__':  # pragma: no cover
    unittest.main(verbosity=2)
