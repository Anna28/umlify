from abc import ABCMeta, abstractmethod


class View(metaclass=ABCMeta):
    @abstractmethod
    def display(self, message):  # pragma: no cover
        pass

    @abstractmethod
    def display_help(self, message):  # pragma: no cover
        pass

    @abstractmethod
    def display_err(self, message):  # pragma: no cover
        pass


class ConsoleView(View):
    def display(self, message):
        print(message)

    def display_help(self, message):
        self.display(message)

    def display_err(self, message):
        self.display(message)