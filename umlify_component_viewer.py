from component_viewer import ComponentViewer
from extractor import Extractor
from diagram_creator import DiagramCreator

try:
    import matplotlib.pyplot as plt
except ImportError:  # pragma: no cover
    raise ImportError("No Module named matplotlib can be found")
try:
    from numpy import arange
except ImportError:  # pragma: no cover
    raise ImportError("No module named numpy can be found")
try:
    from graphviz import Digraph
    from graphviz import ExecutableNotFound
except ImportError:  # pragma: no cover
    raise ImportError("No module named graphviz can be found")

from os import path, remove, makedirs


class UmlifyComponentViewer(ComponentViewer):
    def __init__(self, view=None, input_path=None, output_path=None):
        super().__init__()
        self.v = view
        self.input_path = input_path
        self.output_path = output_path
        self.e = Extractor()
        self.path_provided = False

        if input_path is not None:
            self.e.set_file(input_path)
            self.path_provided = True
        if output_path is None:
            self.output_path = 'output\\'
        if self.e.get_component_dictionary() is not None:
            self.components = list(self.e.get_component_dictionary().values())

    def generate_diagram(self, diagram_creator: DiagramCreator, comp_name=None):
        if isinstance(diagram_creator, DiagramCreator):
            diagram_creator.set_extractor(self.e)

            if not path.exists(self.output_path):
                makedirs(self.output_path)

            diagram_creator.set_output_path(self.output_path)
            diagram_creator.generate_diagram(comp_name)
        else:  # pragma: no cover
            self.v.display_err("can not create that diagram")

    def generate_diagrams(self, diagram_creator: DiagramCreator):
        if isinstance(diagram_creator, DiagramCreator):
            diagram_creator.set_extractor(self.e)

            if not path.exists(self.output_path):
                makedirs(self.output_path)

            diagram_creator.set_output_path(self.output_path)
            for comp_name in self.e.get_component_dictionary().keys():
                diagram_creator.set_output_name(None)  # use default settings for names
                diagram_creator.generate_diagram(comp_name)
        else:  # pragma: no cover
            self.v.display_err("can not create that diagram")
