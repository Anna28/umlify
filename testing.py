import os
import doctest
import shutil
import unittest
import commandline
from test import unittest_extractor, unittest_uvc, unittest_shelf, unittest_database

if __name__ == '__main__':

    # remember current output files
    contents = os.listdir("./output")

    # these tests are in the normal directory
    doctest.testmod(commandline)

    # remove output files created by doctest
    for filename in os.listdir("./output"):
        if filename not in contents:
            file_path = "./output/" + filename
            if os.path.isdir(file_path):
                shutil.rmtree(file_path)
            else:
                os.remove(file_path)

    os.remove("uml_components.db")

    shelf_name = "./test/test_shelf"
    os.remove(shelf_name + ".dat")
    os.remove(shelf_name + ".bak")
    os.remove(shelf_name + ".dir")

    # change path for tests in /test directory
    os.chdir("./test")

    suite = unittest.TestLoader().loadTestsFromModule(unittest_extractor)
    unittest.TextTestRunner(verbosity=0).run(suite)

    suite = unittest.TestLoader().loadTestsFromModule(unittest_uvc)
    unittest.TextTestRunner(verbosity=0).run(suite)

    suite = unittest.TestLoader().loadTestsFromModule(unittest_shelf)
    unittest.TextTestRunner(verbosity=0).run(suite)

    suite = unittest.TestLoader().loadTestsFromModule(unittest_database)
    unittest.TextTestRunner(verbosity=0).run(suite)
