import re
from component import Component
import os
import glob

""" This module receives data from files or folders then
 extracts class names, dependencies, function names and attribute details.
 For each class it creates a new Component object and places the above details in that object.
 This module then outputs a dictionary containing the Component objects for that file/folder    

NEW - Can now:
 - correctly identify if an object eg Extractor() is a default value of an attribute
 - correctly extract and identify attributes such as 'self.setup()'
 - Still work on Classes that do not declare an object eg 'class Database' as opposed to 'class Database(object)

"""


class Extractor(object):

    def __init__(self):
        self.file = ''
        self.componentdict = {}

    def set_file(self, filepath):
        try:
            filestring = filepath
            if os.path.isfile(filestring):
                self._dataextraction(filestring)
            elif os.path.isdir(filestring):
                files = glob.glob(filestring + '/**/*.py', recursive=True)
                for item in files:
                    # print('Item = %s' % item)
                    self._dataextraction(item)
            else:
                print("File not found")
        except FileNotFoundError:  # pragma: no cover
            print('This file cannot be found')

    def _dataextraction(self, filepath):

        with open(filepath, 'r') as sourcefile:
            for line in sourcefile:
                class_name = self._extract_class(line)
                functionname = self._extract_functions(line)
                attributename = self._extract_attributes(line)
                if class_name:
                    comp = self.componentdict.get(class_name[0])
                    if comp is None:
                        comp = Component()
                    comp.set_name(class_name[0])
                    parent = self._extract_parents(line)
                    for item in parent:
                        if item != 'object':
                            parent = self.componentdict.get(item)
                            if parent is None:
                                parent = Component()
                        comp.get_parents().append(parent)
                    self.componentdict[class_name[0]] = comp
                    attributedictionary = {}
                elif functionname:
                    try:
                        functionname = functionname[0]
                        comp.get_functions().append(functionname)
                    except UnboundLocalError as err:  # pragma: no cover
                        print('Class has not been declared for "{0}" function'.format(functionname))
                        print(err)
                        return
                elif attributename:
                    try:
                        if comp.get_functions() == ['__init__']:
                            attrname = attributename[0]
                            datatypedict = self._extract_defaults_datatypes(line)
                            attributedictionary[attrname] = datatypedict
                            comp.set_attributes(attributedictionary)
                    except UnboundLocalError as err:  # pragma: no cover
                        print('Class has not been declared for "{0}" attribute'.format(attrname))
                        print(err)
                        return

    @staticmethod
    def _regex_search(regex, data):
        r = re.compile(regex)
        regexresult = r.findall(data)
        return regexresult

    def _extract_class(self, line):
        regex = '^class\s(\w+)'
        result = self._regex_search(regex, line)
        return result

    def _extract_parents(self, line):
        dependencylist = []
        regex = '^class\s\w+\((.*)\)'
        if not self._regex_search(regex, line):
            return ""
        else:
            dependencynames = self._regex_search(regex, line)[0]
            regexlist = (re.split(r',', dependencynames))
            for item in regexlist:
                if item != 'object':
                    strippeditem = item.strip()
                    dependencylist.append(strippeditem)
            return dependencylist

    def _extract_functions(self, line):
        regex = 'def\s(\w+)'
        return self._regex_search(regex, line)

    def _extract_attributes(self, line):
        regex = '\s{2}self\.(\w+)+\s='
        return self._regex_search(regex, line)

    def _extract_defaults_datatypes(self, line):
        attrdefault = self._extract_attribute_defaults(line)
        if not attrdefault:  # pragma: no cover
            defdatatypesdict = {"": ()}
        else:
            attrtype = self._extract_attribute_datatypes(attrdefault)
            defdatatypesdict = {attrtype: attrdefault}
        return defdatatypesdict

    def _extract_attribute_defaults(self, line):
        regex = '\s{2}self\.\w+\s=\s(.*)'
        attributedefault = self._regex_search(regex, line)
        if attributedefault.__len__() != 0:
            attributedefault = attributedefault[0].replace('"', "")
        return attributedefault

    def _extract_attribute_datatypes(self, attrname):
        regex = '^(.)'
        regex2 = '^[A-Z].+\)$'
        extractedtype = self._regex_search(regex, attrname)
        datatype = extractedtype[0]
        if self._regex_search(regex2, attrname):
            return 'obj'
        elif attrname == "True" or attrname == "False":
            return 'bool'
        elif attrname == "None":
            return 'none'
        elif datatype.isalpha():
            return 'str'
        elif datatype == "'":
            return 'str'
        elif datatype == '{':
            return 'dict'
        elif datatype == '[':
            return 'list'
        elif datatype == '(':
            return 'tuple'
        elif datatype.isdigit():
            return "int"
        else:
            raise ValueError("No data type detected for '{0}'".format(attrname))  # pragma: no cover

    def get_component_dictionary(self):
        return self.componentdict


# test output code to see if modules functioning ok
if __name__ == "__main__":  # pragma: no cover
    e = Extractor()
    e.set_file('component_viewer.py')
    f = e.get_component_dictionary()
    # prints the catual dictionary of objects that are the output of this class
    # print("getcomponentdictionary() output = %s" % f)
    # prints the details that have been recorded for every object in that output
    for obj in f:
        print("Object {0} variables are: {1}".format(obj, vars(f[obj])))
